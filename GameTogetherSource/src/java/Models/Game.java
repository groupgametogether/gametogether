/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author zedqu
 */
public class Game {
    private int gameID;
    private String gameName;
    private String gameImage;
    private String gameDes;

    public Game() {
    }

    public Game(int gameID, String gameName, String gameImage, String gameDes) {
        this.gameID = gameID;
        this.gameName = gameName;
        this.gameImage = gameImage;
        this.gameDes = gameDes;
    }

    public int getGameID() {
        return gameID;
    }

    public void setGameID(int gameID) {
        this.gameID = gameID;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getGameImage() {
        return gameImage;
    }

    public void setGameImage(String gameImage) {
        this.gameImage = gameImage;
    }

    public String getGameDes() {
        return gameDes;
    }

    public void setGameDes(String gameDes) {
        this.gameDes = gameDes;
    }
    
}
