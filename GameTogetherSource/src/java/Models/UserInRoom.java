/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author zedqu
 */
public class UserInRoom {
    private Room room;
    private User user;
    private boolean roleInRoom;
    private String note;

    public UserInRoom() {
    }

    public UserInRoom(Room room, User user, boolean roleInRoom, String note) {
        this.room = room;
        this.user = user;
        this.roleInRoom = roleInRoom;
        this.note = note;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isRoleInRoom() {
        return roleInRoom;
    }

    public void setRoleInRoom(boolean roleInRoom) {
        this.roleInRoom = roleInRoom;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
    
    
}
