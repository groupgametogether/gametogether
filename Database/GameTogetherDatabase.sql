USE [GameTogether]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 26/07/2022 2:10:55 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[accountID] [int] IDENTITY(1,1) NOT NULL,
	[accountCode] [varchar](50) NOT NULL,
	[userName] [varchar](50) NOT NULL,
	[passWord] [varchar](50) NOT NULL,
	[role] [tinyint] NOT NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[accountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Game]    Script Date: 26/07/2022 2:10:55 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Game](
	[gameID] [int] IDENTITY(1,1) NOT NULL,
	[gameCode] [varchar](50) NOT NULL,
	[gameName] [nvarchar](50) NOT NULL,
	[gameImage] [varchar](100) NULL,
	[gameDes] [nvarchar](200) NULL,
 CONSTRAINT [PK_Game] PRIMARY KEY CLUSTERED 
(
	[gameID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Room]    Script Date: 26/07/2022 2:10:55 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Room](
	[roomID] [int] IDENTITY(1,1) NOT NULL,
	[roomCode] [varchar](50) NOT NULL,
	[roomName] [nvarchar](50) NULL,
	[gameId] [int] NULL,
	[numberPersons] [int] NOT NULL,
	[timeCreated] [time](0) NULL,
	[dateCreated] [date] NULL,
	[timeStarted] [time](0) NULL,
	[dateStarted] [date] NULL,
	[status] [nvarchar](50) NULL,
 CONSTRAINT [PK_Room_1] PRIMARY KEY CLUSTERED 
(
	[roomID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 26/07/2022 2:10:55 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[userID] [int] IDENTITY(1,1) NOT NULL,
	[userCode] [varchar](50) NOT NULL,
	[accountID] [int] NOT NULL,
	[userName] [nvarchar](50) NOT NULL,
	[gender] [bit] NOT NULL,
	[dateOfBirth] [date] NOT NULL,
	[email] [varchar](50) NOT NULL,
	[phone] [varchar](50) NOT NULL,
	[rate] [tinyint] NULL,
	[image] [varchar](100) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[userID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserInRoom]    Script Date: 26/07/2022 2:10:55 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserInRoom](
	[roomID] [int] NOT NULL,
	[userID] [int] NOT NULL,
	[roleInRoom] [bit] NOT NULL,
	[note] [nvarchar](50) NULL,
 CONSTRAINT [PK_UserInRoom] PRIMARY KEY CLUSTERED 
(
	[roomID] ASC,
	[userID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Account] ON 

INSERT [dbo].[Account] ([accountID], [accountCode], [userName], [passWord], [role]) VALUES (1, N'ACC11', N'admin', N'admin', 1)
INSERT [dbo].[Account] ([accountID], [accountCode], [userName], [passWord], [role]) VALUES (2, N'ACC01', N'user1', N'user1', 0)
INSERT [dbo].[Account] ([accountID], [accountCode], [userName], [passWord], [role]) VALUES (3, N'ACC02', N'user2', N'user2', 0)
INSERT [dbo].[Account] ([accountID], [accountCode], [userName], [passWord], [role]) VALUES (5, N'ACC03', N'user2', N'user3', 0)
SET IDENTITY_INSERT [dbo].[Account] OFF
GO
SET IDENTITY_INSERT [dbo].[Game] ON 

INSERT [dbo].[Game] ([gameID], [gameCode], [gameName], [gameImage], [gameDes]) VALUES (1, N'LMHT', N'Liên Minh Huyền Thoại', N'img/game/lmht_Logo.png', NULL)
INSERT [dbo].[Game] ([gameID], [gameCode], [gameName], [gameImage], [gameDes]) VALUES (2, N'VALORANT', N'Valorant', N'img/game/valorant_Logo.png', NULL)
INSERT [dbo].[Game] ([gameID], [gameCode], [gameName], [gameImage], [gameDes]) VALUES (3, N'AOE1', N'Đế chế 1', N'img/game/aoe1_Logo.png', NULL)
SET IDENTITY_INSERT [dbo].[Game] OFF
GO
SET IDENTITY_INSERT [dbo].[Room] ON 

INSERT [dbo].[Room] ([roomID], [roomCode], [roomName], [gameId], [numberPersons], [timeCreated], [dateCreated], [timeStarted], [dateStarted], [status]) VALUES (12, N'ROOM_LMHT_1', N'Phòng của User1', 1, 5, CAST(N'21:00:00' AS Time), CAST(N'2022-07-25' AS Date), CAST(N'22:00:00' AS Time), CAST(N'2022-07-25' AS Date), N'Vừa tạo')
INSERT [dbo].[Room] ([roomID], [roomCode], [roomName], [gameId], [numberPersons], [timeCreated], [dateCreated], [timeStarted], [dateStarted], [status]) VALUES (13, N'ROOM_VALORANT_2', N'Phòng của User1', 2, 5, CAST(N'22:00:00' AS Time), CAST(N'2022-07-26' AS Date), CAST(N'22:10:00' AS Time), CAST(N'2022-07-26' AS Date), N'Vừa tạo')
INSERT [dbo].[Room] ([roomID], [roomCode], [roomName], [gameId], [numberPersons], [timeCreated], [dateCreated], [timeStarted], [dateStarted], [status]) VALUES (14, N'ROOM_AOE1_3', N'Phòng của User1', 3, 7, CAST(N'10:00:00' AS Time), CAST(N'2022-07-27' AS Date), CAST(N'15:00:00' AS Time), CAST(N'2022-07-28' AS Date), N'Vừa tạo')
INSERT [dbo].[Room] ([roomID], [roomCode], [roomName], [gameId], [numberPersons], [timeCreated], [dateCreated], [timeStarted], [dateStarted], [status]) VALUES (15, N'ROOM_LMHT_2', N'Phòng của Lam Duc Hieu', 1, 3, CAST(N'13:18:00' AS Time), CAST(N'2022-07-26' AS Date), CAST(N'15:00:00' AS Time), CAST(N'2022-07-26' AS Date), N'Đang chơi')
INSERT [dbo].[Room] ([roomID], [roomCode], [roomName], [gameId], [numberPersons], [timeCreated], [dateCreated], [timeStarted], [dateStarted], [status]) VALUES (16, N'ROOM_AOE1_2', N'Phòng của Nguyen Nhan Trung Kien', 2, 5, CAST(N'13:08:00' AS Time), CAST(N'2022-06-25' AS Date), CAST(N'16:00:00' AS Time), CAST(N'2022-06-27' AS Date), N'Đã chơi xong')
SET IDENTITY_INSERT [dbo].[Room] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([userID], [userCode], [accountID], [userName], [gender], [dateOfBirth], [email], [phone], [rate], [image]) VALUES (1, N'USER11', 1, N'admin', 1, CAST(N'2022-08-02' AS Date), N'zed@gamil.com', N'0366482351', 10, NULL)
INSERT [dbo].[User] ([userID], [userCode], [accountID], [userName], [gender], [dateOfBirth], [email], [phone], [rate], [image]) VALUES (2, N'USER01', 2, N'Tran Minh Quang', 1, CAST(N'2022-02-20' AS Date), N'yasuo@gmail.com', N'0366482351', 10, NULL)
INSERT [dbo].[User] ([userID], [userCode], [accountID], [userName], [gender], [dateOfBirth], [email], [phone], [rate], [image]) VALUES (4, N'USER02', 3, N'Lam Duc Hieu', 0, CAST(N'2001-11-12' AS Date), N'ire@gmail.com', N'0345512551', 9, NULL)
INSERT [dbo].[User] ([userID], [userCode], [accountID], [userName], [gender], [dateOfBirth], [email], [phone], [rate], [image]) VALUES (5, N'USER03', 5, N'Nguyen Nhan Trung Kien', 0, CAST(N'2001-05-19' AS Date), N'joker@gmail.com', N'0352652335', 8, NULL)
SET IDENTITY_INSERT [dbo].[User] OFF
GO
INSERT [dbo].[UserInRoom] ([roomID], [userID], [roleInRoom], [note]) VALUES (12, 2, 1, NULL)
INSERT [dbo].[UserInRoom] ([roomID], [userID], [roleInRoom], [note]) VALUES (12, 4, 0, N'Xin 5p ăn cơm')
INSERT [dbo].[UserInRoom] ([roomID], [userID], [roleInRoom], [note]) VALUES (12, 5, 0, NULL)
INSERT [dbo].[UserInRoom] ([roomID], [userID], [roleInRoom], [note]) VALUES (13, 2, 1, NULL)
INSERT [dbo].[UserInRoom] ([roomID], [userID], [roleInRoom], [note]) VALUES (13, 4, 0, NULL)
INSERT [dbo].[UserInRoom] ([roomID], [userID], [roleInRoom], [note]) VALUES (14, 2, 1, NULL)
INSERT [dbo].[UserInRoom] ([roomID], [userID], [roleInRoom], [note]) VALUES (14, 5, 0, NULL)
GO
ALTER TABLE [dbo].[Room]  WITH CHECK ADD  CONSTRAINT [FK_Room_Game] FOREIGN KEY([gameId])
REFERENCES [dbo].[Game] ([gameID])
GO
ALTER TABLE [dbo].[Room] CHECK CONSTRAINT [FK_Room_Game]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Account] FOREIGN KEY([accountID])
REFERENCES [dbo].[Account] ([accountID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Account]
GO
ALTER TABLE [dbo].[UserInRoom]  WITH CHECK ADD  CONSTRAINT [FK_UserInRoom_User] FOREIGN KEY([userID])
REFERENCES [dbo].[User] ([userID])
GO
ALTER TABLE [dbo].[UserInRoom] CHECK CONSTRAINT [FK_UserInRoom_User]
GO
